const connect = require("../db/connect");
module.exports = class scheduleController {
  static async createSchedule(req, res) {
    const { dateStart, dateEnd, days, user, classroom, timeStart, timeEnd } =
      req.body;
    console.log(req.body);
    // Verifica se todos os campos estão preenchidos
    if (
      !dateStart ||
      !dateEnd ||
      !days ||
      !user ||
      !classroom ||
      !timeStart ||
      !timeEnd
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Converta o array days em uma string separada por vírgulas
    const daysString = days.map((day) => `${day}`).join(", ");
    console.log(daysString);
    try {
      const overlapQuery = `
    SELECT * FROM schedule
    WHERE 
        classroom = '${classroom}'
        AND (
            (dateStart <= '${dateEnd}' AND dateEnd >= '${dateStart}')
        )
        AND (
            (timeStart <= '${timeEnd}' AND timeEnd >= '${timeStart}')
        )
        AND (
            (days LIKE '%Seg%' AND '${daysString}' LIKE '%Seg%') OR
            (days LIKE '%Ter%' AND '${daysString}' LIKE '%Ter%') OR
            (days LIKE '%Qua%' AND '${daysString}' LIKE '%Qua%') OR 
            (days LIKE '%Qui%' AND '${daysString}' LIKE '%Qui%') OR
            (days LIKE '%Sex%' AND '${daysString}' LIKE '%Sex%') OR
            (days LIKE '%Sab%' AND '${daysString}' LIKE '%Sab%')
        )`;

      connect.query(overlapQuery, function (err, results) {
        if (err) {
          console.log(err);
          return res.status(500).json({ error: "Erro ao verificar agendamento existente" });
        }

        // Se a consulta retornar algum resultado, significa que já existe um agendamento
        if (results.length > 0) {
          return res.status(400).json({error:"Já existe um agendamento para os mesmos dias, sala e horários",
            });
        }

        // Caso contrário, prossegue com a inserção na tabela
        const insertQuery = `
                INSERT INTO schedule (dateStart, dateEnd, days, user, classroom, timeStart, timeEnd)
                VALUES (
                    '${dateStart}',
                    '${dateEnd}',
                    '${daysString}',
                    '${user}',
                    '${classroom}',
                    '${timeStart}',
                    '${timeEnd}'
                )
            `;

        // Executa a consulta de inserção
        connect.query(insertQuery, function (err) {
          if (err) {
            console.log(err);
            return res.status(500).json({ error: "Erro ao cadastrar agendamento" });
          }
          console.log("Agendamento cadastrado com sucesso");
          return res.status(201).json({ message: "Agendamento cadastrado com sucesso" });
        });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
};
